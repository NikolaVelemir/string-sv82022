#pragma once
#include <iostream>
#include <ostream>
class MyString {
private:
	char* string;
	int size;
public:
	MyString(); 
	MyString(const char string[]);
	MyString(MyString* other);

	int getSize() const;
	char* getString() const;

	//kopira objekte klase, drugi u prvi i za rezultat ima novi objekat klase MyString
	static MyString myStrcpyS(MyString &destination,const MyString& source);
	//kopira niz karatkera, drugi u prvi i za rezultat ima kopiju 
	static char*  myStrcpy(char* destination, const char* source);
	//kopira objekte klase, drugi u prvi
	static void myStrcpySIplc(MyString& destination, const MyString& source);

	
	static int myStrlen(const MyString& destination);
	static int myStrlen(const char* string);
	
	//kontkatenira znakovni niz, drugi na prvi, rezultat mu je konkatenirani niz
	static char* myStrcat(char* destination, const char* source);
	//kontkatenira znakovni niz objekata, drugi na prvi, 
	//rezultat mu je novi objekat sa konktateniranim znakovnim nizom
	static MyString myStrcatS(MyString& destination, const MyString& source);
	//kontkatenira znakovni niz objekata, drugi na prvi, 
	static void myStrcatSIplc(MyString& destination, const MyString& source);


	MyString &operator=(const char string[]);
	friend std::ostream& operator<<( std::ostream &os, const MyString a);
	friend MyString operator+( MyString& a,  MyString& b);
};