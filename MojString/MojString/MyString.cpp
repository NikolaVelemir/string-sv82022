#include "MyString.h"
#include <iostream>
#include <cstring>

MyString::MyString():size(0),string(nullptr)
{
	this->string = new char[1];
	this->string[0] = '\0';
}

MyString::MyString(const char string[])
{
	if (string == nullptr) {
		this->string = new char[1];
		this->string[0] = '\0';
	}
	else {
		this->string = new char[MyString::myStrlen(string) + 1];
		this->size = MyString:: myStrlen(string) + 1;
		
		MyString::myStrcpy(this->string, string);
		this->string[MyString::myStrlen(string)] = '\0';
	}
}

MyString::MyString(MyString* other)
{
	this->size = other->size;
	this->string = other->string;
}


MyString& MyString::operator=(const char string[])
{
	MyString *rez = new MyString(string);
	this->size = rez->size;
	this->string = rez->string;
	return *this;
}

int MyString::getSize() const 
{
	return this->size;
}

char* MyString::getString()const 
{
	return this->string;
}



MyString MyString::myStrcpyS(MyString& destination,const MyString& source)
{
	char* ptr = destination.getString();
	char* dest = destination.getString();
	char* src = source.getString();

	while (*src != '\0') {
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
	destination = MyString(ptr);
	return destination;
}

char* MyString::myStrcpy(char* destination,const char* source)
{
	char* ptr = destination;

	while (*source != '\0') {
		*destination = *source;
		destination++;
		source++;

	}
	*destination = '\0';
	return ptr;
}

void MyString::myStrcpySIplc(MyString& destination, const MyString& source)
{
	char* ptr = destination.getString();
	char* dest = destination.getString();
	char* src = source.getString();
	while (*src != '\0') {
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
	destination = new MyString(ptr);
}

int MyString::myStrlen(const MyString& destination)
{
	return destination.getSize();
}

int MyString::myStrlen(const char* string)
{
	int i = 0;
	
	while (*string != '\0') {
		string++;
		i++;
	}
	return i;
}

char* MyString::myStrcat(char* destination, const char* source)
{
	char* ptr = destination;
	while (*destination != '\0') {
		destination++;
	}
	while (*source != '\0') {
		*destination = *source;
		destination++;
		source++;
	}
	*destination = '\0';
	return ptr;
}

MyString MyString::myStrcatS(MyString& destination, const MyString& source)
{
	char* ptr = destination.getString();
	char* dest = destination.getString();
	char* src = source.getString();

	while (*dest != '\0') {
		dest++;
	}
	while (*src != '\0') {
		*dest = *src;
		src++;
		dest++;
	}
	*dest = '\0';
	destination = MyString(ptr);
	return MyString(ptr);


}

void MyString::myStrcatSIplc(MyString& destination, const MyString& source)
{
	char* ptr = destination.getString();
	char* dest = destination.getString();
	char* src = destination.getString();
	while (*dest != '\0') {
		dest++;
	}
	while (*src != '\0') {
		*dest = *src;
		dest++;
		src++;
	}
	*dest = '\0';
	destination = new MyString(ptr);
}

std::ostream& operator<<( std::ostream& os, const MyString a)
{

	
	os << a.string;
	return os;

}

MyString operator+( MyString& a,  MyString& b) 
{
	char* pom = new char[a.getSize()];
	pom = MyString::myStrcpy(pom, a.getString());
	MyString::myStrcat(pom, b.getString());
	return MyString(pom);
}
