#include "MyString.h"
#include <iostream>

int main() {
	MyString string = "Momo";
	std::cout << string << std::endl;
	MyString string2 = " i Dodir";

	MyString string3 = MyString::myStrcpyS(string, string2);
	std::cout << string3 << std::endl;
	
	string = "Momo";

	MyString string4 = MyString::myStrcatS(string, string2);
	std::cout << string4 << std::endl;

	string = "Momo";
	std::cout << string + string2 << std::endl;
	


	return 0;
}